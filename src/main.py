#!/usr/bin/env python
'''
Program: Mini Camelot
'''

__author__ = "Ho Sung Kim"

import human_player
import cpu_player
import board
import game

def main():
    '''This main function will run the Mini Camelot game.'''

    human = human_player.Human("white")
    cpu = cpu_player.CPU("black")
    game_board = board.Board()

    # Checks if game is running
    game_running = True
    # Human player choose who goes first
    response = game.play_first()
    # Human player's turn (True) or cpu turn (False)
    human_turn = True if response == 'Y' else False
    # Populate board with pieces
    game_board.update_board(human.pieces, cpu.pieces)
    game_board.print_board()

    while game_running:
        if human_turn:
            print "Human turn"
            #Human makes a move
            human.move()
            #Game board updates along with human piece location
            human_turn = False
        else:
            print "CPU turn"
            #CPU makes a move (Alpha-beta --> eval fxn)
            cpu.move()
            #Game board updates along with cpu location
            human_turn = True
        game_board.print_board()
        #Check game status (WIN/DRAW/LOSE)

# Make file executable
if __name__ == "__main__":
    main()
