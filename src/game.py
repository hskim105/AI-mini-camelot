'''
Module: Game
'''

def game_status():
    '''
    Game status checks whether the human or cpu player won, lost, or there is a draw
    '''
    #Check if both of opponent's castles are captured
    #Capture all of opponent's pieces while retaining 2 or more of my own pieces
    #Draw when both players only have 1 piece or less
    #Show alert message when there is a draw
    return "win"

def play_first():
    while True:
        response = raw_input("Would you like to play first? (Y/N): ").upper()
        if response == 'Y' or response == 'N':
            break
    return response
