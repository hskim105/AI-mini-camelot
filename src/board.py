'''
Module: Board
'''

class Board():
    '''
    Board class docstring
    '''

    def __init__(self):
        self.max_row = 14
        self.max_column = 8
        self.game_board = []
        self.bad_position = {0 : [0, 1, 2, 5, 6, 7],
                             1 : [0, 1, 6, 7],
                             2 : [0, 7],
                             11 : [0, 7],
                             12 : [0, 1, 6, 7],
                             13 : [0, 1, 2, 5, 6, 7]}
        self.initialize_board()

    def initialize_board(self):
        '''Initializes the game board with correct boarders'''
        for x in range(self.max_row):
            self.game_board.append([])
            for y in range(self.max_column):
                if x in self.bad_position.keys() and y in self.bad_position[x]:
                    self.game_board[x].append("XX")
                else:
                    self.game_board[x].append("__")

    def update_board(self, human_pieces, cpu_pieces):
        '''Updates the board by taking inputs from human and cpu pieces'''
        for x in range(len(cpu_pieces)):
            self.game_board[cpu_pieces[x]["row"]][cpu_pieces[x]["column"]] = 'B' + str(cpu_pieces[x]["number"])
        for y in range(len(human_pieces)):
            self.game_board[human_pieces[y]["row"]][human_pieces[y]["column"]] = 'W' + str(human_pieces[y]["number"])

    def print_board(self):
        '''prints the entire game board to console'''
        for x in range(self.max_row):
            for y in range(self.max_column):
                print self.game_board[x][y],
            print

    def clean_position(self, x, y):
        '''Removes human or cpu's old position from the board after they make a move'''
        self.game_board[x][y] = '_'

    def get_bad_position(self):
        return self.bad_position
