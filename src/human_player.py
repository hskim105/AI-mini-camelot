'''
Module: Human
'''

class Human():
    '''
    Human class docstring
    '''

    def __init__(self, color):
        self.color = color
        self.pieces = []

        self.initial_position = {8 : [3, 4],
                                 9 : [2, 3, 4, 5]}
        #Initalize pieces
        self.initialize_pieces(color)

    def move(self):
        print "Human move turn"
        possible_moves = {}
        #Capturing move (Mandatory. MUst perform)
        #Cantering move and plain moves
        # self.plain_move(

    def create_piece(self, number, color, row, column, captured=0):
        '''Creates a piece by using a dictionary and storing the parameters'''
        piece = {"number" : number,
                 "color" : color,
                 "row" : row,
                 "column" : column,
                 "captured" : captured}
        return piece

    def initialize_pieces(self, color):
        '''Initialize pieces array using initial position dictionary'''
        number = 0  #Used to specify piece number and must match with its position in pieces list
        for key in self.initial_position.iterkeys():
            for pos in range(len(self.initial_position[key])):
                self.pieces.append(self.create_piece(number, color, key, self.initial_position[key][pos]))
                number += 1

    def plain_move(self, piece):
        pass
